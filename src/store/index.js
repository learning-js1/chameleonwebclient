import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers";
import {signal} from './helpers/createSignalRMiddleware';
import { composeWithDevTools } from 'redux-devtools-extension';

const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(signal)
      )
);

export default store;
