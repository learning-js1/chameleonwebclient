import { SET_PLAYERS, ADD_PLAYER } from "./actionTypes";

export const setPlayersAction = players => ({
    type: SET_PLAYERS,
    players
});

export const addPlayerAction = player => ({
    type: ADD_PLAYER,
    player
});