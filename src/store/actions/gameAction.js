import { NEW_GAME } from "./actionTypes";

export const newGameAction = () => ({
    type: NEW_GAME,
});