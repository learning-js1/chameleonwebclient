
import { setStorage } from "../../utils/localStorage";

const { SET_SESSION, CLEAR_SESSION } = require("../actions/actionTypes")

const initialState = {
    nickname: "",
    gamePin: "",
    isHost: false
};

const sessionReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case SET_SESSION:
            return action.session;
        case CLEAR_SESSION: {
            setStorage("Player_Session", initialState);
            return initialState;
        }
        default:
            return state;
    }
}

export default sessionReducer;
