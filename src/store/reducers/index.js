import { combineReducers } from "redux";
import sessionReducer from "./sessionReducer";
import gameReducer from "./gameReducer";

export default combineReducers({
    session: sessionReducer,
    game: gameReducer
});
