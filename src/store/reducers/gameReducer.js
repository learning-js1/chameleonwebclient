const { SET_PLAYERS, ADD_PLAYER, NEW_GAME } = require("../actions/actionTypes")

const initialState ={ players: [] };

const gameReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case SET_PLAYERS:
            return action.players;
        case ADD_PLAYER:
            return {...state,
                players: [...state.players, action.player]}
        case NEW_GAME:
            return initialState;
        default:
            return state;
    }
}

export default gameReducer;