import { HubConnectionBuilder, withCallbacks, signalMiddleware } from 'redux-signalr';
import environment from '../../environment/environment';
import {addPlayerAction, setPlayersAction} from '../actions/playersAction';

export const connection = new HubConnectionBuilder()
.withUrl(`${environment.hubUrl}`)
.withAutomaticReconnect()
.build();


const callbacks = withCallbacks()
    .add('JoinGame', (player) => (dispatch) => {
        console.log('new player joined', player);
        dispatch(addPlayerAction(player))
        dispatch(updateLobby());
    })
    .add('CurrentLobby', (players) => (dispatch) => {
        console.log('New list of players', players);
        dispatch(setPlayersAction({players: players}));
    })
 
export const updateLobby = () => (dispatch, getState, invoke) => {
    console.log("----updateLobby----");
    let players = getState().game.players;
    console.log("invoking updateLobby with players: ", players)
    invoke('UpdateLobby', players)
};

export const joinRoom = (player) => (dispatch, getState, invoke) => {
    invoke('JoinRoom', player)
};

export const signal = signalMiddleware({
    callbacks,
    connection,
})