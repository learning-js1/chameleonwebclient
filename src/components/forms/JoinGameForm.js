import React, { useState } from "react";
import { useHistory } from 'react-router-dom';
import { setStorage } from "../../utils/localStorage";
import { useDispatch } from "react-redux";
import { setSessionAction } from "../../store/actions/sessionAction";
import { joinRoom } from "../../store/helpers/createSignalRMiddleware";


const JoinGameForm = () => {

    const history = useHistory();
    const dispatch = useDispatch();

    //Setting initial values for game and username
    const [username, setUsername] = useState("");
    const [gamePin, setGamePin] = useState("");


    const onUsernameChanged = event => setUsername(event.target.value.trim());
    const onGamePinChanged = event => setGamePin(event.target.value.trim());

    const OnJoinGameClicked = () => {
        console.log("Username:", username);
        console.log("Game Pin:", gamePin);
        const session = { nickname:username, gamePin:gamePin, isHost:false };
        setStorage("Player_Session", session);
        dispatch(setSessionAction(session));
        dispatch(joinRoom(session));
        history.push("/game");
        
    }

    return (
        <form>
            <div>
                <label>Username: </label>
                <input type="text" placeholder="Enter your username" onChange={onUsernameChanged} />
            </div>

            <div>
                <label>Game id: </label>
                <input type="text" placeholder="Enter game pin" onChange={onGamePinChanged} />
            </div>

            <div>
                <button type="button" onClick={OnJoinGameClicked}>Join game</button>
            </div>


        </form>
    )
};

export default JoinGameForm;