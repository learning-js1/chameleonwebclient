import React, { useState } from "react";
import { useHistory } from 'react-router-dom';
import { getPin } from '../../api/GameService';
import { setStorage } from "../../utils/localStorage";
import { useDispatch } from "react-redux";
import { setSessionAction } from "../../store/actions/sessionAction";
import { joinRoom } from "../../store/helpers/createSignalRMiddleware";
import { newGameAction} from '../../store/actions/gameAction';

const CreateGameForm = () => {

    const dispatch = useDispatch();
    const history = useHistory();
    const [username, setUsername] = useState("");
    const onUsernameChanged = event => setUsername(event.target.value.trim());
    const OnCreateGameClicked = () => {
        dispatch(newGameAction())
        console.log("Username:", username);

        initNewGame().then(pin => {
            const session = { nickname:username, gamePin:pin, isHost:true };
            setStorage("Player_Session", session);
            dispatch(setSessionAction(session));
            dispatch(joinRoom(session));

            history.push("/game")
        });


    }

    const initNewGame = async () => {
        try {
            let pin = await getPin();
            return pin.toString();
        } catch (e) {
            console.log('initializing new game failed.', e)
        }
    }


    return (
        <form>
            <div>
                <label>Username: </label>
                <input type="text" placeholder="Enter your username" onChange={onUsernameChanged} />
            </div>

            <div>
                <button type="button" onClick={OnCreateGameClicked}>Create game</button>
            </div>


        </form>
    )
};

export default CreateGameForm;