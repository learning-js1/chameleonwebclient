import React from "react";
import { useHistory } from "react-router-dom";

const Navbar = () => {

    const history = useHistory();

    const onNavClicked = () => {
        history.replace("/dashboard");
    }

    return (
        <nav className="navbar navbar-dark bg-success">
            <span type="button" onClick={onNavClicked}>Chamillionaire</span>
        </nav>
    )
};

export default Navbar;