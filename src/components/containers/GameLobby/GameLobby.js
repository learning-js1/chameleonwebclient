import GameWindow from './GameWindow/GameWindow';

const GameLobby = () => {

    return (
        <div>
            <h3>Game lobby!</h3>
                <GameWindow />
        </div>
    )
};

export default GameLobby;