import React from "react";
import CreateGameForm from "../../forms/CreateGameForm";
import { Link } from "react-router-dom";

const CreateGame = () => {

    return (
        <div>
            <h3>Create a new game!</h3>
            <CreateGameForm />
            <Link to="/dashboard">Changed your mind? Go back!</Link>

        </div>
    )
};

export default CreateGame;