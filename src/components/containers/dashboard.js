import React from "react";
import { useHistory } from "react-router-dom";

const Dashboard = () => {

    const history = useHistory();

    const onJoinClick = () => {
        history.replace("/join");
    }

    const onCreateClick = () => {
        history.replace("/create");
    }

    return (
        <div>
            <h1>Welcome to the Chamillionaire</h1>
            <p>You can either create a new game or join an existing one.</p>
            <form>
                <div>
                    <button type="button" onClick={onJoinClick}>JOIN GAME</button>
                </div>
                <div>
                    <button type="button" onClick={onCreateClick}>CREATE GAME</button>
                </div>
            </form>
        </div>

    )
};

export default Dashboard;