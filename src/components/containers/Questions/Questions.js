import React, { useState } from "react";
import { getQuestion } from "../../../api/QuestionService";

const Questions = () => {

    const [isLoading, setIsLoading] = useState(false);
    const [isReady, setIsReady] = useState(false);
    const [category, setCategory] = useState("");
    const [alternatives, setAlternatives] = useState([]);

    const onPlayerClicked = async () => {
        alert("The secret word is: ");
    };

    const onChameleonClicked = async () => {
        alert("You are the Chamillionaire");
    };

    const onButtonClicked = async () => {

        setIsLoading(true);
        setIsReady(false);
        let questionResult;

        try {
            questionResult = await getQuestion();
            setCategory(questionResult.category);
            setAlternatives(() => [...questionResult.alternatives.split(", ")]);
        } catch (error) {
            console.log(error);
        } finally {
            setIsLoading(false);
            setIsReady(true);
        }


    };




    return (
        <div>
            <h1>We are now playing the Chamillionaire game!</h1>
            <p>Enjoy your time and trust nobody</p>

            {isReady && <ul className="list-group">
                <li className="list-group-item list-group-item-success">{category}</li>
                {alternatives.map((item, index) => {
                    return <li className="list-group-item list-group-item-action" key={index}>{item}</li>
                })}
            </ul>}

            <button disabled={isLoading} type="button" onClick={onButtonClicked}>Next Question</button>
            <button type="button" onClick={onPlayerClicked}>Show Card</button>
            <button type="button" onClick={onChameleonClicked}>Show Card</button>

            {isLoading && <p>Loading questions...</p>}
        </div>
    )
};

export default Questions;