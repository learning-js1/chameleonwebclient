import React from "react";
import JoinGameForm from "../../forms/JoinGameForm";
import { Link } from "react-router-dom";

const JoinGame = () => {

    return (
        <div>
            <h3>Join new game!</h3>
            <JoinGameForm />
            <Link to="/dashboard">Changed your mind? Go back!</Link>
        </div>
    )
};

export default JoinGame;

