import environment from '../environment/environment';
import axios from "axios";



export const getPin = async () => {
    return axios.get(`${environment.apiUrl}Init/newgame`)
        .then(response => response.data);
}

export const updateLobbyService = async(lobby) => {
    return axios.post(`${environment.apiUrl}Init/game`, lobby)
}




