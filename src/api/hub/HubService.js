import { HubConnectionBuilder } from '@microsoft/signalr';
import environment from '../../environment/environment';


export const hub = (function () {
    const connection = new HubConnectionBuilder()
        .withUrl(`${environment.hubUrl}`)
        .withAutomaticReconnect()
        .build();
    

    function start(player) {
        connection.start()
            .then(result => {
                console.log('Connected!');
                connection.invoke('JoinRoom', player);
                if (player.isHost) {
                    connection.on('JoinGame', (newPlayer) => {
                        //add player to players in redux
                        console.log("add player to lobby in redux", newPlayer);
                    });
                } else {
                    connection.on('CurrentLobby', (playersFromHost) => {
                        //Set players in redux
                        console.log("Set lobby in redux.", playersFromHost);
                    })
                }
            })
            .catch(e => console.log('Connection failed: ', e));
    }
    function stop(){
        connection.stop();
    }
    return {
        start: start,
         stop: stop
        };
})();