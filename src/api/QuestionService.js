import environment from '../environment/environment';
import axios from "axios";


export const getQuestion = async () => {
    return axios.get(`${environment.apiUrl}question/random`)
        .then(response => response.data);
}



